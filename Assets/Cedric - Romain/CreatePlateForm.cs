﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePlateForm : MonoBehaviour {
    //*********************
    //Floors principale
    //**********************
    private List<GameObject> floors;
    private const int START_POSITION_X = -6;
    private const int START_POSITION_Y = -3;
    private const int AMOUNT_OF_CUBES = 13;
    private int firstIndex = 0;
    private int lastIndex = AMOUNT_OF_CUBES - 1;
    private int middle = Mathf.FloorToInt(AMOUNT_OF_CUBES / 2) + 1;
    public float nextTimeActionDeadPrincipal = 10f;
    public float nextTimeActionFullPrincipal = 5f;
    public float nextTimeActionHalfPrincipal = 2f;
    public  float DELAY_DEAD = 10f;
   

    //*********************
    //Floors left
    //**********************
    private List<GameObject> floors_top_left;
    private const int ST_POS_X_LEFT = -7;
    private const int ST_POS_Y_LEFT = 0;
    private const int AMOUNT_OF_CUBES_LEFT = 7;
    private int firstIndexLeft = 0;
    private int lastIndexLeft = AMOUNT_OF_CUBES_LEFT - 1;
    private int middleLeft = Mathf.FloorToInt(AMOUNT_OF_CUBES_LEFT / 2) + 1;
    public float nextTimeActionDeadLeft = 30f;
    public float nextTimeActionFullLeft = 15f;
    public float nextTimeActionHalfLeft = 7f;
    public  float DELAY_DEAD_LEFT = 30f;


    //*********************
    //Floors right
    //**********************
    private List<GameObject> floors_top_right;
    private const int ST_POS_X_RIGHT = 2;
    private const int ST_POS_Y_RIGHT = 0;
    private const int AMOUNT_OF_CUBES_RIGHT = 5;
    private int firstIndexRight = 0;
    private int lastIndexRight = AMOUNT_OF_CUBES_RIGHT - 1;
    private int middleRight= Mathf.FloorToInt(AMOUNT_OF_CUBES_RIGHT / 2) + 1;
    public float nextTimeActionDeadRight = 20f;
    public float nextTimeActionFullRight = 13f;
    public float nextTimeActionHalfRight = 6f;
    public  float DELAY_DEAD_RIGHT = 20f;

    private float currentTime;



    // Sprite information.
    public Texture2D spriteTexture;
	public int numberOfTiles;

	// Loaded sprites
	private Sprite[] tiles;

    // ********************
    // Public methods
    // ********************
    // Use this for initialization
    void Start () {
        currentTime = 0f;
        tiles = Resources.LoadAll<Sprite>(spriteTexture.name);

        floors = SetupFloor(START_POSITION_X, START_POSITION_Y, AMOUNT_OF_CUBES);
        floors_top_left = SetupFloor(ST_POS_X_LEFT, ST_POS_Y_LEFT, AMOUNT_OF_CUBES_LEFT);
        floors_top_right = SetupFloor(ST_POS_X_RIGHT, ST_POS_Y_RIGHT, AMOUNT_OF_CUBES_RIGHT);

       
    }
	
	// Update is called once per frame
	void Update () {
        currentTime += Time.deltaTime;
        CycleLife(ref floors,ref firstIndex, ref lastIndex, ref nextTimeActionDeadPrincipal, ref nextTimeActionHalfPrincipal, ref nextTimeActionFullPrincipal, DELAY_DEAD, middle );
        CycleLife(ref floors_top_left, ref firstIndexLeft, ref lastIndexLeft, ref nextTimeActionDeadLeft, ref nextTimeActionHalfLeft, ref nextTimeActionFullLeft, DELAY_DEAD_LEFT,middleLeft);
        CycleLife(ref floors_top_right, ref firstIndexRight, ref lastIndexRight, ref nextTimeActionDeadRight, ref nextTimeActionHalfRight, ref nextTimeActionFullRight, DELAY_DEAD_RIGHT,middleRight);

    }

    private GameObject getCube (float x, float y, int indexTile)
    {
        GameObject cube = new GameObject();

		cube.AddComponent<BoxCollider>();
		SpriteRenderer spriteRenderer = cube.AddComponent<SpriteRenderer>();
		spriteRenderer.sprite = tiles[indexTile];
		spriteRenderer.drawMode = SpriteDrawMode.Sliced;
		spriteRenderer.size = new Vector2(1.0f, 1.0f);
        cube.AddComponent<Rigidbody>();
        cube.GetComponent<Rigidbody>().useGravity = false;
        cube.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        cube.transform.position = new Vector3(x, y, 0);

        return cube;
    }
    
    private List<GameObject> SetupFloor(int x, int y, int nbCube )
    {

        List<GameObject> floor = new List<GameObject>();
        GameObject currentCube;


        currentCube = getCube(x, y, 0);
        for (int i = 0; i < nbCube; i++)
        {
            //CENTER LEFT leaf 
            if ( i < (nbCube - 1) /2  && i != 0)
            {
                 currentCube = getCube(x + (float)i , y,1);
            }
            
            //CENTER RIGHT leaf
            if ( i > (nbCube - 1) / 2 && i != (nbCube - 1))
            {
                 currentCube = getCube(x + (float)i , y, 3);
            }

            //MIDDLE LEAF 
            if ( i == Mathf.FloorToInt(nbCube / 2) )
            {
                currentCube = getCube(x + (float)i , y, 2);
            }

            //RIGHT leaf
            if ( i  == nbCube - 1 )
            {
                currentCube = getCube(x + (float)i, y, 4);
            }

            floor.Add(currentCube);
        }

        return floor;
        
    }

    private void CycleLife(ref List<GameObject> floor,ref int firstElementIndex,ref int lastElementIndex, ref float nextTimeActionDead, ref float nextTimeActionHalf, ref float nextTimeActionFull, float DELAY_DEAD, int mid)
    {
        if (currentTime > nextTimeActionDead && currentTime < DELAY_DEAD * mid)
        {
            nextTimeActionDead += DELAY_DEAD;
            floor[firstElementIndex].GetComponent<Rigidbody>().useGravity = true;
            floor[lastElementIndex].GetComponent<Rigidbody>().useGravity = true;
            floor[firstElementIndex].GetComponent<Collider>().isTrigger = true;
            floor[lastElementIndex].GetComponent<Collider>().isTrigger = true;
            floor[firstElementIndex].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            floor[lastElementIndex].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            firstElementIndex++;
            lastElementIndex--;
        }

        if (currentTime > nextTimeActionHalf && currentTime < DELAY_DEAD * mid)
        {
            nextTimeActionHalf += DELAY_DEAD;
            if (firstElementIndex == 0)
            {
                floor[firstElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[5];
                floor[lastElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[9];
            }
            else if ( firstElementIndex == mid - 1)
            {
                floor[firstElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[7];
            }
            else
            {
                floor[firstElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[6];
                floor[lastElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[8];
            }
        }

        if (currentTime > nextTimeActionFull && currentTime < DELAY_DEAD * mid)
        {
            nextTimeActionFull += DELAY_DEAD;
            if (firstElementIndex == 0)
            {
                floor[firstElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[10];
                floor[lastElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[14];
            }
            else if ( firstElementIndex == mid - 1)
            {
                floor[firstElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[12];
            }
            else
            {
                floor[firstElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[11];
                floor[lastElementIndex].GetComponent<SpriteRenderer>().sprite = tiles[13];
            }
        }
    }
}