﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSway_cedric : MonoBehaviour {


	public float swayDistance = 3.0f;
	public float swayDuration = 1.0f;
	private float direction = 1.0f;
	private Vector3 initialPosition;

	private float initalStartTime;
	// Use this for initialization
	void Start () {
		initalStartTime = Time.time;
		initialPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		float currentTime = Time.time - initalStartTime;
		float percentCompleted = currentTime / swayDuration;

		Vector3 endPosition = new Vector3(direction * swayDistance + initialPosition.x, initialPosition.y, initialPosition.z);
		transform.position = Vector3.Lerp(transform.position, endPosition, percentCompleted); 

		if(percentCompleted > 1.0) {
			initalStartTime = Time.time;
			direction = direction * -1.0f;
		}
	}
}
