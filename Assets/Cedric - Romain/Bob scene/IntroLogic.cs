﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroLogic_cedric : MonoBehaviour {
	public void StartGame () {
		SceneManager.LoadScene("Game", LoadSceneMode.Single);
	}

	public void ExitGame () {
		Application.Quit();
	}
}
