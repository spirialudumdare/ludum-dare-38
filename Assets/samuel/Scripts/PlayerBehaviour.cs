﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {

	// must be the same as the "dead" parameter in ProjectSettings/InputManager.asset
	public float dead_zone;

	public bool keyboard_controlled;
	public string keyboard_axis;
	public string jump_key;
	public string punch_key;
	public bool joystick_controlled;
	public String joystick_name;
	public float x_orientation = 1.0f;
	public float speed;
	public float jump_velocity;
	public float downwards_force_trigger;
	public float extra_downwards_force;
	public int max_charge;
	public float slow_charging_animation_speed;
	public float fast_charging_animation_speed;
	public float ultra_fast_charging_animation_speed;
	public float slow_punch_velocity;
	public float fast_punch_velocity;
	public float post_punch_velocity;
	public int slow_punch_duration;
	public int fast_punch_duration;
	public float recovery_force;
	public float recovery_up_force;
	public int recovery_duration;

	private Rigidbody rb;
	private CollisionDetector groundDetector;
	private CollisionDetector leftWallDetector;
	private CollisionDetector rightWallDetector;
	private Animator animator;
	private AudioSource[] punchingSounds;
	private AudioSource punchMissesSound;
	private AudioSource punchHitsSound;
	private AudioSource chargingSound;
	private AudioSource maxChargeSound;
	private AudioSource jumpSound;

	private int punching = 0;
	private int recovering = 0;
	private int charging = 0;

	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		groundDetector = transform.FindChild ("GroundDetector").gameObject.GetComponent<CollisionDetector>();
		leftWallDetector = transform.FindChild ("LeftWallDetector").gameObject.GetComponent<CollisionDetector>();
		rightWallDetector = transform.FindChild ("RightWallDetector").gameObject.GetComponent<CollisionDetector>();
		animator = transform.FindChild ("Sprite").gameObject.GetComponent<Animator> ();

		punchingSounds = new AudioSource[]{
			transform.FindChild("Punching1").gameObject.GetComponent<AudioSource>(),
			transform.FindChild("Punching2").gameObject.GetComponent<AudioSource>(),
			transform.FindChild("Punching3").gameObject.GetComponent<AudioSource>(),
			transform.FindChild("Punching4").gameObject.GetComponent<AudioSource>(),
		};

		punchMissesSound = GameObject.Find ("PunchMisses").GetComponent<AudioSource> ();
		punchHitsSound = GameObject.Find ("PunchHits").GetComponent<AudioSource> ();
		chargingSound = GameObject.Find ("Charging").GetComponent<AudioSource> ();
		maxChargeSound = GameObject.Find ("MaxCharge").GetComponent<AudioSource> ();
		jumpSound = GameObject.Find ("Jump").GetComponent<AudioSource> ();
	}

	public bool isStanding() {
		return groundDetector.isColliding ();
	}

	bool GetPunchKeyDown () {
		if (keyboard_controlled) {
			if (Input.GetKeyDown (punch_key)) return true;
		}

		if (joystick_controlled) {
			// gamepad button "A" is "button 18" on mac and "button 2" otherwise, see http://wiki.unity3d.com/index.php?title=Xbox360Controller
			if (Input.GetKeyDown (joystick_name + " button 2")) return true;
			if (Input.GetKeyDown (joystick_name + " button 18")) return true;
		}

		return false;
	}

	bool GetPunchKeyUp () {
		if (keyboard_controlled) {
			if (Input.GetKeyUp (punch_key)) return true;
		}

		if (joystick_controlled) {
			// gamepad button "A" is "button 18" on mac and "button 2" otherwise, see http://wiki.unity3d.com/index.php?title=Xbox360Controller
			if (Input.GetKeyUp (joystick_name + " button 2")) return true;
			if (Input.GetKeyUp (joystick_name + " button 18")) return true;
		}

		return false;
	}

	bool GetJumpKeyDown() {
		if (keyboard_controlled) {
			if (Input.GetKeyDown (jump_key)) return true;
		}

		if (joystick_controlled) {
			// gamepad button "A" is "button 16" on mac and "button 0" otherwise, see http://wiki.unity3d.com/index.php?title=Xbox360Controller
			if (Input.GetKeyDown (joystick_name + " button 0")) return true;
			if (Input.GetKeyDown (joystick_name + " button 16")) return true;
		}

		return false;
	}

	float GetXAxis() {
		if (keyboard_controlled) {
			float dx = Input.GetAxis (keyboard_axis);
			if (dx != 0.0f) {
				// in case of conflict, keyboard presses have priority over the gamepad.
				// don't add both or the player could get double speed!
				return dx;
			}
		}

		if (joystick_controlled) {
			return Input.GetAxis (joystick_name + " Horizontal");
		}

		return 0.0f;
	}

	void Update() {
		if (GetPunchKeyDown() && punching == 0 && recovering == 0 && charging == 0) {
			charging = 1;
			chargingSound.Play ();
		}

		if (GetPunchKeyUp() && punching == 0 && recovering == 0 && charging > 0) {
			float fraction = charging * 1.0f / max_charge;
			float x_velocity = Mathf.Lerp (slow_punch_velocity, fast_punch_velocity, fraction);
			rb.velocity = new Vector3(x_orientation * x_velocity, rb.velocity.y, rb.velocity.z);
			punching = (int) Mathf.Lerp(slow_punch_duration, fast_punch_duration, fraction);
			charging = 0;

			int random_index = UnityEngine.Random.Range (0, punchingSounds.Length);
			chargingSound.Stop ();
			maxChargeSound.Stop ();
			punchingSounds[random_index].Play();
		}

		if (GetJumpKeyDown() && groundDetector.isColliding()) {
			rb.velocity = new Vector3 (rb.velocity.x, jump_velocity, rb.velocity.z);
			jumpSound.Play ();
		}
	}

	void FixedUpdate ()
	{
		if (charging > 0 && charging < max_charge) {
			charging += 1;

			if (charging == max_charge) {
				maxChargeSound.Play ();
			}
		}
		if (recovering > 0) {
			recovering -= 1;
		}
		if (punching > 0) {
			// can't change direction while punching
			punching -= 1;

			if (punching == 0 && Math.Abs(rb.velocity.x) > post_punch_velocity) {
				rb.velocity = new Vector3 (x_orientation * post_punch_velocity, rb.velocity.y, rb.velocity.z);
			}
		} else {
			// move left/right

			float moveHorizontal = GetXAxis ();

			// remember in which direction to punch
			if (moveHorizontal > dead_zone) {
				x_orientation = 1.0f;
			}
			if (moveHorizontal < -dead_zone) {
				x_orientation = -1.0f;
			}

			// prevent the player from "sticking" to a wall by walking towards it
			if (moveHorizontal > 0.0f && rightWallDetector.isColliding()) moveHorizontal = 0.0f;
			if (moveHorizontal < 0.0f && leftWallDetector.isColliding()) moveHorizontal = 0.0f;

			if (Math.Abs(moveHorizontal) > dead_zone) {
				float stamina = (recovery_duration - recovering) * 1.0f / recovery_duration;
				rb.velocity = new Vector3 (stamina * moveHorizontal * speed, rb.velocity.y, rb.velocity.z);
			}
		}

		// stick to the XY plane
		rb.position = new Vector3 (rb.position.x, rb.position.y, 0.0f);

		if (rb.velocity.y < downwards_force_trigger) {
			// artificially increase gravity on the way down
			rb.AddForce(new Vector3(0.0f, -extra_downwards_force, 0.0f));
		}

		// orient sprite
		animator.transform.localPosition = new Vector3 (-x_orientation * Math.Abs (animator.transform.localPosition.x), animator.transform.localPosition.y, animator.transform.localPosition.z);
		animator.transform.localScale = new Vector3 (x_orientation * Math.Abs (animator.transform.localScale.x), animator.transform.localScale.y, animator.transform.localScale.z);

		// animate sprite
		if (charging > 0) {
			animator.Play ("Charge");
			if (charging == max_charge) {
				animator.speed = ultra_fast_charging_animation_speed;
			} else {
				float fraction = charging * 1.0f / max_charge;
				animator.speed = Mathf.Lerp (slow_charging_animation_speed * 1.0f, fast_charging_animation_speed * 1.0f, fraction);
			}
		} else if (punching > 0) {
			animator.Play ("Punch");
			animator.speed = 1;
		} else if (recovering > 0) {
			animator.Play ("Recovery");
			animator.speed = 1;
		} else if (groundDetector.isColliding() && Math.Abs(rb.velocity.x) > dead_zone) {
			animator.Play ("Walk");
			animator.speed = 1;
		} else {
			animator.Play ("Idle");
			animator.speed = 1;
		}
	}

	void OnTriggerEnter(Collider collider)
	{
		if (punching > 0) {
			CollisionDetector wall = x_orientation > 0.0f ? rightWallDetector : leftWallDetector;
			if (wall.isColliding ()) {
				foreach (AudioSource punchingSound in punchingSounds) {
					punchingSound.Stop ();
				}

				PlayerBehaviour otherPlayer = collider.gameObject.GetComponent<PlayerBehaviour> ();
				if (otherPlayer != null) {
					otherPlayer.rb.velocity = Vector3.zero;
					otherPlayer.rb.AddForce (new Vector3 (x_orientation * otherPlayer.recovery_force, otherPlayer.recovery_up_force, 0.0f));
					otherPlayer.recovering = otherPlayer.recovery_duration;
					otherPlayer.punching = 0;
					otherPlayer.charging = 0;
					punchHitsSound.Play ();
				} else {
					punchMissesSound.Play ();
				}

				rb.velocity = Vector3.zero;
				rb.AddForce (new Vector3 (-x_orientation * recovery_force, recovery_up_force, 0.0f));
				recovering = recovery_duration;
				punching = 0;
				charging = 0;
			}
		}
	}
}