﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsBehaviour : MonoBehaviour {
	// globals set at the end of the intro, and used at the beginning of each fight
	public int dropdownIndex;
	public bool p1UsesKeyboardOnly;
	public bool p2UsesKeyboardToo;
	public int nbPlayers;

	void Start () {
		// persist between scene reloads
		DontDestroyOnLoad(transform.gameObject);
	}
}
