﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour {
	public bool enableLogging;

	private HashSet<Collider> collisionSet = new HashSet<Collider>();

	public bool isColliding() {
		return collisionSet.Count > 0;
	}

	void OnTriggerEnter(Collider collider)
	{
		if (enableLogging) print ("now colliding with " + collider);
		collisionSet.Add (collider);
	}

	void OnTriggerExit(Collider collider)
	{
		if (enableLogging) print ("no longer colliding with " + collider);
		collisionSet.Remove (collider);
	}
}
