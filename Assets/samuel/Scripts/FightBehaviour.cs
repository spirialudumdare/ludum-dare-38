﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FightBehaviour : MonoBehaviour {
	public int intro_duration;
	public int death_duration;
	public int completion_duration;
	public float cameraParallaxRate;
	public float nearParallaxRate;
	public float farParallaxRate;
	public float maxParallax;
	public float cameraElasticity;

	private SettingsBehaviour settings;
	private HashSet<GameObject> players;
	private GameObject camera;
	private GameObject nearBackgroundLayer;
	private GameObject farBackgroundLayer;
	private GameObject killLine;
	private CanvasRenderer winner;
	private CanvasRenderer draw;
	private CanvasRenderer death;
	private AudioSource announceSound;
	private AudioSource backgroundMusic;
	private AudioSource deathSound;
	private AudioSource winSound;

	private GameObject explosion;

	private int introducing = 0;
	private int dying = 0;
	private int completing = 0;

	private float currentPlayerOffset = 0.0f;
	private float initialPlayerOffset;
	private float initialCameraOffset;
	private float initialNearBackgroundOffset;
	private float initialFarBackgroundOffset;

	void Start () {
		GameObject settingsObject = GameObject.Find ("Settings");
		if (settingsObject == null) {
			// the scene was started directly, without going through the intro first,
			// so we don't have access to the global Settings object.
			// Use the scene-specific object instead.
			settingsObject = GameObject.Find ("BackupSettings");
		}
		settings = settingsObject.GetComponent<SettingsBehaviour> ();

		players = new HashSet<GameObject>(GameObject.FindGameObjectsWithTag ("Player"));
		int playerNumber = 1;
		int joystickNumber = 1;
		HashSet<GameObject> nonPlayingPlayers = new HashSet<GameObject> ();
		foreach (GameObject player in players) {
			PlayerBehaviour playerBehaviour = player.GetComponent<PlayerBehaviour> ();

			if (playerNumber == 1) {
				// doesn't hurt to also allow the keyboard even if they asked for gamepad
				playerBehaviour.keyboard_controlled = true;
				playerBehaviour.keyboard_axis = "Arrows Horizontal";

				if (settings.p2UsesKeyboardToo) {
					// two players share the same keyboard, use the cramped keymapping
					playerBehaviour.jump_key = "n";
					playerBehaviour.punch_key = "m";
				} else {
					// only one keyboard-controlled player, use the comfortable keymapping
					playerBehaviour.jump_key = "z";
					playerBehaviour.punch_key = "x";
				}

				if (settings.p1UsesKeyboardOnly) {
					playerBehaviour.joystick_controlled = false;
				} else {
					playerBehaviour.joystick_controlled = true;
					playerBehaviour.joystick_name = "joystick " + joystickNumber;
					joystickNumber += 1;
				}
			} else if (playerNumber == 2 && settings.p2UsesKeyboardToo) {
				playerBehaviour.keyboard_controlled = true;
				playerBehaviour.joystick_controlled = false;
				playerBehaviour.keyboard_axis = "WASD Horizontal";
				playerBehaviour.jump_key = "1";
				playerBehaviour.punch_key = "q";
			} else {
				playerBehaviour.keyboard_controlled = false;
				playerBehaviour.joystick_controlled = true;
				playerBehaviour.joystick_name = "joystick " + joystickNumber;
				joystickNumber += 1;
			}

			if (playerNumber > settings.nbPlayers) {
				nonPlayingPlayers.Add (player);
			}
			playerNumber += 1;
		}
		foreach (GameObject player in nonPlayingPlayers) {
			players.Remove (player);
			Destroy (player);
		}

		explosion = GameObject.Find("SmokePillard");
		camera = GameObject.Find ("Main Camera");
		nearBackgroundLayer = GameObject.Find ("NearBackGround");
		farBackgroundLayer = GameObject.Find ("FarBackGround");
		killLine = GameObject.Find ("KillLine");

		winner = GameObject.Find ("Winner").GetComponent<CanvasRenderer> ();
		draw = GameObject.Find ("Draw").GetComponent<CanvasRenderer> ();
		winner.SetAlpha (0.0f);
		draw.SetAlpha (0.0f);

		announceSound = GameObject.Find ("Announce").GetComponent<AudioSource> ();
		backgroundMusic = GameObject.Find ("BackgroundMusic").GetComponent<AudioSource> ();
		deathSound = GameObject.Find ("Death").GetComponent<AudioSource> ();
		winSound = GameObject.Find ("Win").GetComponent<AudioSource> ();

		initialPlayerOffset = AveragePlayerPosition ();
		initialCameraOffset = camera.transform.position.x;
		initialNearBackgroundOffset = nearBackgroundLayer.transform.position.x;
		initialFarBackgroundOffset = farBackgroundLayer.transform.position.x;

		announceSound.Play ();
		introducing = intro_duration;
	}

	float AveragePlayerPosition() {
		float offset = 0.0f;
		foreach(GameObject player in players) {
			offset += player.transform.position.x;
		}
		if (players.Count == 0) return currentPlayerOffset;
		offset /= players.Count;

		if (offset > maxParallax) return maxParallax;
		if (offset < -maxParallax) return -maxParallax;
		return offset;
	}

	void Update() {
		if (Input.GetKeyUp ("escape")) {
			SceneManager.LoadScene ("Intro", LoadSceneMode.Single);
		}

		float desiredOffset = AveragePlayerPosition () - initialPlayerOffset;
		float offset = Mathf.Lerp (currentPlayerOffset, desiredOffset, cameraElasticity);
		currentPlayerOffset = offset;
		camera.transform.position = new Vector3(initialCameraOffset + cameraParallaxRate * offset, camera.transform.position.y, camera.transform.position.z);
		nearBackgroundLayer.transform.position = new Vector3(initialNearBackgroundOffset + nearParallaxRate * offset, nearBackgroundLayer.transform.position.y, nearBackgroundLayer.transform.position.z);
		farBackgroundLayer.transform.position = new Vector3(initialFarBackgroundOffset + farParallaxRate * offset, farBackgroundLayer.transform.position.y, farBackgroundLayer.transform.position.z);
	}

	void FixedUpdate () {
		if (dying > 0) {
			dying -= 1;
		}

		if (completing > 0) {
			completing -= 1;

			if (completing == 0) {
				SceneManager.LoadScene (SceneManager.GetActiveScene().name);
			}
		}

		if (introducing > 0) {
			introducing -= 1;

			if (introducing == 0) {
				backgroundMusic.Play ();
			}
		}

		if (completing == 0) {
			HashSet<GameObject> deadPlayers = new HashSet<GameObject> ();
			foreach (GameObject player in players) {
				if (player.transform.position.y < killLine.transform.position.y) {
					showExplosionForPlayer(player);
					// player died
					deadPlayers.Add (player);
				}
			}
			foreach (GameObject deadPlayer in deadPlayers) {
				players.Remove (deadPlayer);
				//Destroy (deadPlayer);
				dying = death_duration;
				deathSound.PlayOneShot (deathSound.clip);
			}

			if (dying == 0) {
				if (players.Count == 1) {
					foreach (GameObject player in players) {
						if (player.GetComponent<PlayerBehaviour> ().isStanding ()) {
							// player wins
							dying = 0;
							completing = completion_duration;
							winner.SetAlpha (1.0f);
							backgroundMusic.Stop ();
							winSound.Play ();
						}
					}
				} else if (players.Count == 0) {
					// it's a draw
					dying = 0;
					completing = completion_duration;
					draw.SetAlpha (1.0f);
					backgroundMusic.Stop ();
					announceSound.Play ();
				}
			}
		}
	}

	void showExplosionForPlayer(GameObject player) {

		float dist = (player.transform.position - Camera.main.transform.position).z;
		float leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		float rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;

		explosion.transform.position = new Vector3 (Mathf.Clamp(player.transform.position.x, leftBorder, rightBorder), -3.5f, 0.0f);

		ParticleSystem exp = explosion.GetComponent<ParticleSystem>();
		exp.Play();
		//Destroy(gameObject, exp.duration);
	}
}
