﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroLogic : MonoBehaviour {
	private SettingsBehaviour settings;
	private Dropdown dropdown;

	void Start() {
		settings = GameObject.Find ("Settings").GetComponent<SettingsBehaviour> ();
		dropdown = GameObject.FindObjectOfType<Dropdown> ();
		dropdown.value = settings.dropdownIndex;
	}

	public void StartGame () {
		settings.dropdownIndex = dropdown.value;
		if (dropdown.value == 0) {
			settings.p1UsesKeyboardOnly = true;
			settings.p2UsesKeyboardToo = false;
			settings.nbPlayers = 2;
		} else if (dropdown.value == 1) {
			settings.p1UsesKeyboardOnly = true;
			settings.p2UsesKeyboardToo = false;
			settings.nbPlayers = 3;
		} else if (dropdown.value == 2) {
			settings.p1UsesKeyboardOnly = true;
			settings.p2UsesKeyboardToo = false;
			settings.nbPlayers = 4;
		} else if (dropdown.value == 3) {
			settings.p1UsesKeyboardOnly = false;
			settings.p2UsesKeyboardToo = false;
			settings.nbPlayers = 2;
		} else if (dropdown.value == 4) {
			settings.p1UsesKeyboardOnly = false;
			settings.p2UsesKeyboardToo = false;
			settings.nbPlayers = 3;
		} else if (dropdown.value == 5) {
			settings.p1UsesKeyboardOnly = false;
			settings.p2UsesKeyboardToo = false;
			settings.nbPlayers = 4;
		} else if (dropdown.value == 6) {
			settings.p1UsesKeyboardOnly = true;
			settings.p2UsesKeyboardToo = true;
			settings.nbPlayers = 2;
		} else if (dropdown.value == 7) {
			settings.p1UsesKeyboardOnly = true;
			settings.p2UsesKeyboardToo = true;
			settings.nbPlayers = 3;
		} else if (dropdown.value == 8) {
			settings.p1UsesKeyboardOnly = true;
			settings.p2UsesKeyboardToo = true;
			settings.nbPlayers = 4;
		}

		SceneManager.LoadScene("Game", LoadSceneMode.Single);
	}

	public void ExitGame () {
		Application.Quit();
	}
}
